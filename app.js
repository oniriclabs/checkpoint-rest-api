const express = require("express");
const app = express();
const bodyparser = require("body-parser");
const qs = require('query-string');
const got = require('got');

require('dotenv').config();

const port = process.env.PORT || 3200;

// API v5 helix URLs
const twitch_base_url = 'https://api.twitch.tv/helix'
const twitch_stream_url = `${twitch_base_url}/streams?`
const twitch_game_url = `${twitch_base_url}/games?`

// required headers
const twitch_header = {
    headers: {
        'Content-Type': 'application/json',
        'Client-ID': process.env.TWITCH_CLIENT_ID
    },
    gzip: true
}

//Middle ware

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: false }));

/**
 *  END POINT - check server is running
 */
app.get("/running", (req, res) => {
    res.status(200).send("Rest API is running");
});

/**
 *  END POINT - fetch
 */
app.get("/streaming/:id", async (req, res) => {

    let url = build_url(twitch_stream_url, {
        "user_login": req.params.id
    });

    try {
        // check if user is online
        let response = await got(url, twitch_header);

        // parse result
        let result = JSON.parse(response.body);

        // if user is offline
        if (result.data.length <= 0) {

            res.status(200).send({
                "status": "offline",
                "playing": null
            });
            return
        }

        // grab game ID
        const game_id = result.data[0].game_id

        url = build_url(twitch_game_url, {
            "id": game_id
        });

        // fetch game details
        response = await got(url, twitch_header);

        // parse result
        result = JSON.parse(response.body);

        res.status(200).send({
            "status": "online",
            "playing": result.data[0]
        });

    } catch (error) {

        console.log(error.response.body);
        //=> 'Internal server error ...'
        res.status(500).send(`error: unable to fetch user`);
    }
});

app.listen(port, () => {
    console.log(`running at port ${port}`);
});

function build_url(base, params) {
    return base + qs.stringify(params)
}