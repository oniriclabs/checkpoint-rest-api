# Checkpoint REST API with Node.js

An API that given a Twitch streamer ID, determine if they are currently streaming or not
If they are streaming, what game are they currently playing

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

What things you need to install the software and how to install them

* Download [Node](https://nodejs.org/en/) and npm
* `npm install` to install all our node dependencies

## Running the server
* run `npm start` to start the server.
* visit 'http://127.0.0.1:3200/running' to verify the server is running
* visit 'http://127.0.0.1:3200/streaming/twitch_name' to fetch what the twitch user is currently streaming (if they are streaming at all)

## Other commands
* run `npm run eslint` to lint the project
* run `npm run live` for live reload

## Built With
* [Express](https://expressjs.com/) - Fast, unopinionated, minimalist web framework for Node.js
* [Node.js](https://nodejs.org/en/) - Node.js® is a JavaScript runtime built on Chrome's V8 JavaScript engine.]